#include <windows.h>
#include <tchar.h> 
#include <strsafe.h>
#include <vector>
#include <string>
#pragma comment(lib, "User32.lib")
using namespace std;

bool findFileNameAndBytes(vector<unsigned long>& bytes, vector<wstring>& filenames);

bool findFileNameAndBytes(char* directory, vector<unsigned long>& bytes, vector<string>& filenames);