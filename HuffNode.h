#pragma once
#include <stddef.h>

class CHuffNode
{
public:
	unsigned char c;
	long nFreq;
	CHuffNode *Left;
	CHuffNode *Right;

	CHuffNode();
	CHuffNode(char c, long nFreq);
	~CHuffNode();
	long getFreq(){ return nFreq; };
};
