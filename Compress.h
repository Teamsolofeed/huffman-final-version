#pragma once
#include "HuffTree.h"
#include "ListFile.h"
#include "gotoxy.h"
#include <string>
#include <fstream>
#include <stdlib.h>
using namespace std;

#define SIGNATURE "ABCDEFG"

struct FileInfo{
	char m_FileName[256];			//Ten File toi da 256 ki tu
	unsigned __int64 m_SizeBefore;	//Size truoc khi nen
	unsigned __int64 m_SizeEnd;		//Size sau khi nen
	unsigned __int64 m_PosCharFirst;//Vi tri ki tu dau tien
	unsigned __int64 m_PosCharLast;	//Vi tri ki tu cuoi cung
	char m_BitZero;					//Bit du
};
typedef struct FileInfo FILEINFO;

struct header{
	char m_Signature[8];					//Ma nhan dang
	unsigned int m_Amount = 0;					//So luong file
	unsigned __int64 m_TableCharacter[256];	//Bang thong ke
	FILEINFO *aFile = NULL;					//Mang dong cac struc FILEINFO
};
typedef struct header HEADER;

class CCompress : public CHuffTree
{
private:
	HEADER m_Hd;
	char* m_NameFolder;
	char* m_NameFileOut;

	string* m_aEnCoding;

	int m_nbits;
	int m_current_byte;
public:
	CCompress();
	CCompress(char* NameFolder, char* NameFileOut);
	~CCompress();

	void process();
	void countCharacter();
	void writeHeader(ofstream& fo);
	void bitout(ofstream& fo, char bit);
};

