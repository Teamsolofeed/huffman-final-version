#pragma once
#include "HuffNode.h"

class CPriorityQueue
{
	CHuffNode *Item;
	int Rear;
	int MaxSize;

	void heapify(int i);
public:
	CPriorityQueue();
	CPriorityQueue(int Size);
	~CPriorityQueue();

	bool isEmpty();
	bool insert(CHuffNode& NewItem);
	CHuffNode deleteMin();
	bool minValue(CHuffNode &Item);

	int getSize();
};

