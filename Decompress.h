#pragma once
#include "HuffTree.h"
#include "HuffNode.h"
#include "Compress.h"
#include <fstream>
#include "gotoxy.h"
using namespace std;

struct BitBuffer{
	char content[8];
	int post;
};
typedef struct BitBuffer BITBUFFER;

class CDecompress : public CHuffTree
{
	HEADER m_Hd;
	char* m_FileIn;
	BITBUFFER m_Buffer;
	string* m_aEncodingTable;

	void readHeader();
	void checkSum(unsigned int pos);
	unsigned long getFileSize(char* FileName);
public:
	CDecompress();
	CDecompress(char* FileIn);
	~CDecompress();

	void convertCharToBinary(unsigned char c);//Doi char sang binarry
	void allFile();	//Giai nen tat ca cac file
	void view();	//Xem file nen
	void chooseFile(char *str);	//Giai nen cac file trong chuoi str//	vd	-[1,3,6]
};

