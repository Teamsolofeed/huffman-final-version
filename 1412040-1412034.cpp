#include "Compress.h"
#include "Decompress.h"
#include <time.h>
#include <iostream>
using namespace std;

void main(int argc, char* argv[]){
	if (argc == 3 || argc == 4){
		if (!strcmp(argv[1], "-e")){
			CCompress cpr(argv[2], argv[3]);
			cpr.process();
		}
		else if (!strcmp(argv[1], "-v")){
			CDecompress dcpr(argv[2]);
			dcpr.view();
		}
		else if (!strcmp(argv[1], "-d")){
			CDecompress dcpr(argv[3]);
			if (!strcmp(argv[2], "-all"))
				dcpr.allFile();
			else
				dcpr.chooseFile(argv[2]);
		}
		else
			cout << "Sai tham so dong lenh";
	}
	else
		cout << "Sai tham so dong lenh";
}

//void main()
//{
//	/*CCompress cpr("E:\\ThienBao\\test", "out.huf");
//	cpr.process();*/
//	CDecompress dcpr("out.huf");
//	dcpr.view();
//	cin.get();
//}