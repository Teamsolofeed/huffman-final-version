#include "PriorityQueue.h"
#include <iostream>
using namespace std;


CPriorityQueue::CPriorityQueue()
{
}

CPriorityQueue::CPriorityQueue(int Size)
{
	Item = new CHuffNode[Size];
	Rear = 0;
	MaxSize = Size;
}

CPriorityQueue::~CPriorityQueue()
{
	delete[]Item;
	Rear = 0;
}

void CPriorityQueue::heapify(int i)
{
	CHuffNode saved = Item[i];
	while (i < Rear / 2) {
		int child = 2 * i + 1;
		if (child < Rear - 1){
			if (Item[child].nFreq > Item[child + 1].nFreq)
				child++;
		}
		if (saved.nFreq < Item[child].nFreq/* || (saved.nFreq == Item[child].nFreq && saved.c < Item[child].c)*/)
			break;
		Item[i] = Item[child];
		i = child;
	}
	Item[i] = saved;
}

bool CPriorityQueue::isEmpty()
{
	return Rear == 0;
}

bool CPriorityQueue::insert(CHuffNode& NewItem)
{
	if (Rear == MaxSize){
		return false;
	}
	else{
		Item[Rear] = NewItem;
		int i = Rear;
		while (((i > 0) && Item[i].nFreq < Item[(i - 1) / 2].nFreq)
			/*|| (Item[i].nFreq == Item[(i - 1) / 2].nFreq && Item[i].c < Item[(i - 1) / 2].c)*/){
			CHuffNode temp = Item[i];
			Item[i] = Item[(i - 1) / 2];
			Item[(i - 1) / 2] = temp;
			i = (i - 1) / 2;
		}
		Rear++;
		return true;
	}
}

CHuffNode CPriorityQueue::deleteMin()
{
	if (!isEmpty()){
		CHuffNode temp = Item[0];
		this->Item[0] = this->Item[Rear - 1];
		Rear--;
		heapify(0);
		return temp;
	}
}

bool CPriorityQueue::minValue(CHuffNode &Item)
{
	if (isEmpty()){
		return false;
	}
	else{
		Item = this->Item[0];
		return true;
	}
}

int CPriorityQueue::getSize()
{
	return Rear;
}