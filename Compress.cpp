#include "Compress.h"


CCompress::CCompress()
{
}

CCompress::CCompress(char* NameFolder, char* NameFileOut)
{
	m_NameFolder = NameFolder;
	m_NameFileOut = NameFileOut;

	m_nbits = m_current_byte = 0;

	strcpy(m_Hd.m_Signature, SIGNATURE);
}

CCompress::~CCompress()
{
}

void CCompress::process()
{
	system("cls");
	cout << "Ten File";
	gotoxy(70, 1);
	cout << "Xu li";
	countCharacter();//Lap bang thong ke va tao bang ma bit

	ofstream fo(m_NameFileOut, ios::out | ios::binary);
	writeHeader(fo);	//Ghi header ao (chua day du thong tin) muc dich la chua cho :v

	for (int i = 0; i < m_Hd.m_Amount; i++){
		unsigned long X = m_Hd.aFile[i].m_SizeBefore;

		string FilePath = string(m_NameFolder) + '\\' + m_Hd.aFile[i].m_FileName;//Lay duong dan file
		ifstream fi(FilePath, ios::in | ios::binary);//Mo tung file de nen

		m_Hd.aFile[i].m_PosCharFirst = fo.tellp(); //Lay vi tri ki tu bat dau

		gotoxy(50, i + 1);
		cout << endl << m_Hd.aFile[i].m_FileName;
		gotoxy(70, i + 2);
		cout << "100%";
		unsigned char ch;
		while (true){
			ch = fi.get();
			if (fi.eof())
				break;
			unsigned char *s = (unsigned char*)m_aEnCoding[ch].c_str();
			for (; *s; s++)
				this->bitout(fo, *s);
		}
		char Zeros = 0;
		if (m_nbits){
			Zeros = 8 - m_nbits;
			while (m_nbits)
				this->bitout(fo, '0');
			fo.put(unsigned char(Zeros));
		}
		else
			fo.put((char)Zeros);

		m_Hd.aFile[i].m_PosCharLast = fo.tellp();	//Lay vi tri ki tu ket thuc
		m_Hd.aFile[i].m_BitZero = Zeros;	//Lay bit du
		m_Hd.aFile[i].m_SizeEnd = m_Hd.aFile[i].m_PosCharLast - m_Hd.aFile[i].m_PosCharFirst;	//Lay size sau khi nen
		fi.close();
	}

	writeHeader(fo);	//Ghi header da day du thong tin
	cout << endl <<  endl << "Done!";
	fo.close();

	delete[]m_Hd.aFile;
}

void CCompress::countCharacter()
{
	//Lay ten tap tin trong thu muc va size tung file
	vector<string> ListFileName;
	vector<unsigned long> ListFileSize;
	if (!findFileNameAndBytes(m_NameFolder, ListFileSize, ListFileName)){
		cout << "Khong tim thay thu muc";
		exit(0);
	}

	m_Hd.m_Amount = ListFileName.size();	//gan so luong file
	m_Hd.aFile = new FILEINFO[m_Hd.m_Amount];
	//khoi tao cac phan tu trong bang thong ke = 0
	for (int i = 0; i < 256; i++)
		m_Hd.m_TableCharacter[i] = 0;
	//khoi tao file va lap bang thong ke
	for (int i = 0; i < m_Hd.m_Amount; i++){
		strcpy(m_Hd.aFile[i].m_FileName, ListFileName[i].c_str());
		m_Hd.aFile[i].m_SizeBefore = ListFileSize[i];
		m_Hd.aFile[i].m_BitZero = m_Hd.aFile[i].m_PosCharFirst = m_Hd.aFile[i].m_PosCharLast = m_Hd.aFile[i].m_SizeEnd = 0;
		string FilePath = string(m_NameFolder) + '\\' + m_Hd.aFile[i].m_FileName;//Lay duong dan file
		ifstream fi;
		fi.open(FilePath, ios::in | ios::binary);
		unsigned char c;
		while (true){
			c = fi.get();
			if (fi.eof())
				break;
			m_Hd.m_TableCharacter[c]++;
		}
		fi.close();
	}
	m_aEnCoding = CreateEnCodingTable(m_Hd.m_TableCharacter);
}

void CCompress::writeHeader(ofstream& fo)
{
	long temp;
	fo.clear();
	fo.seekp(0, ios::beg);

	fo.write((char*)&m_Hd.m_Signature, sizeof(char)* 8);
	fo.write((char*)&m_Hd.m_Amount, sizeof(int));
	fo.write((char*)&m_Hd.m_TableCharacter, sizeof(m_Hd.m_TableCharacter));
	for (int i = 0; i < m_Hd.m_Amount; i++){
		fo.write((char*)&m_Hd.aFile[i], sizeof(m_Hd.aFile[i]));
	}
}

void CCompress::bitout(ofstream& fo, char bit)
{
	m_current_byte <<= 1;


	if (bit == '1')
		m_current_byte |= 1;
	m_nbits++;

	if (m_nbits == 8)
	{
		fo.put((unsigned char)m_current_byte);
		m_nbits = 0;
		m_current_byte = 0;
	}
}