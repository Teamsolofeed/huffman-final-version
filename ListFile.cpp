#include "ListFile.h"

bool findFileNameAndBytes(char* directory, vector<unsigned long>& bytes, vector<string>& filenames){
	//struct store file attributes
	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize; //64 bits signed integer
	char folder[MAX_PATH];

	//handle file
	HANDLE hFind = INVALID_HANDLE_VALUE;
	size_t length_of_arg;

	// Check that the input path plus 3 is not longer than MAX_PATH.
	// Three characters are for the "\*" plus NULL appended below.
	StringCchLength(directory, MAX_PATH, &length_of_arg);

	if (length_of_arg > (MAX_PATH - 3)){
		return false;
	}

	// Prepare string for use with FindFile functions.  First, copy the
	// string to a buffer, then append '\*' to the directory name.
	StringCchCopy(folder, MAX_PATH, directory);
	StringCchCat(folder, MAX_PATH, TEXT("\\*"));

	// Find the first file in the directory.
	hFind = FindFirstFile(folder, &ffd);

	//if there is no file in the folder
	if (INVALID_HANDLE_VALUE == hFind){
		return false;
	}

	do{
		if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)){
			//find each file size
			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;

			//push name and size to respective vector
			bytes.push_back(filesize.QuadPart);
			filenames.push_back(ffd.cFileName);
		}

	} while (FindNextFile(hFind, &ffd) != 0);

	if (filenames.size() == 0){
		return false;
	}

	return true;
}