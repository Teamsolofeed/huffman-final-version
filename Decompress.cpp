#include "Decompress.h"


CDecompress::CDecompress()
{
}

CDecompress::CDecompress(char* FileIn)
{
	m_FileIn = FileIn;
}

CDecompress::~CDecompress()
{
}

void CDecompress::convertCharToBinary(unsigned char c)
{
	int i;
	for (i = 0; i < 8; ++i)
		m_Buffer.content[i] = '0';
	m_Buffer.content[8] = '\0';
	i = 7;
	while (i >= 0)
	{
		m_Buffer.content[i] = (c % 2) + '0';
		c = c / 2;
		i--;
	}
	m_Buffer.post = 0;
}

void CDecompress::allFile()
{
	system("cls");
	cout << "Ten File";
	gotoxy(70, 1);
	cout << "Xu li";
	gotoxy(80, 1);
	cout << "Checksum" << endl;

	readHeader();
	ifstream fi(m_FileIn, ios::in | ios::binary);
	//Tao cay huffman
	CHuffNode *root = CreateHuffTree(m_Hd.m_TableCharacter);
	//Bat dau giai nen
	for (int i = 0; i < m_Hd.m_Amount; i++){
		ofstream fo(m_Hd.aFile[i].m_FileName, ios::out | ios::binary);

		unsigned long start = m_Hd.aFile[i].m_PosCharFirst;
		unsigned long end = m_Hd.aFile[i].m_PosCharLast;
		fi.seekg(end - 1, ios::beg);	//Dua con tro file ve byte ke cuoi de doc byte cuoi co the chua bit du

		unsigned char c;
		fi.read((char*)&c, sizeof(unsigned char));

		int full_bytes = end - start - 2;

		fi.seekg(start, ios::beg);	//Dua con tro file ve data bat dau

		unsigned char ch = fi.get();
		convertCharToBinary(ch);
		CHuffNode *t = root;
		for (int i = 1; i <= full_bytes;)
		{
			if (t->Left == NULL && t->Right == NULL)
			{
				fo.put(t->c);
				t = root;
			}
			else
			{
				if (m_Buffer.post < 8 && m_Buffer.content[m_Buffer.post] == '0')
					t = t->Left;
				if (m_Buffer.post < 8 && m_Buffer.content[m_Buffer.post] == '1')
					t = t->Right;
				if (m_Buffer.post == 8)
				{
					ch = fi.get();
					convertCharToBinary(ch);
					++i;
					m_Buffer.post = -1;
				}
				m_Buffer.post++;

			}

		}
		if (full_bytes >= 0){
			int remain = (int(c) == 0) ? 8 : 8 - int(c);
			for (; m_Buffer.post <= remain;)
			{
				if (t->Left == NULL && t->Right == NULL)
				{
					fo.put(t->c);
					t = root;
				}
				else
				{
					if (m_Buffer.post < 8 && m_Buffer.content[m_Buffer.post] == '0')
						t = t->Left;
					if (m_Buffer.post < 8 && m_Buffer.content[m_Buffer.post] == '1')
						t = t->Right;
					m_Buffer.post++;
				}
			}
		}
		fo.close();
		checkSum(i);
		cout << endl;
	}
	fi.close();
	cout << endl << endl << "Done!";
	delete []m_Hd.aFile;
	m_Hd.aFile = NULL;
}

void  CDecompress::chooseFile(char *str)
{
	system("cls");
	cout << "Ten File";
	gotoxy(70, 1);
	cout << "Xu li";
	gotoxy(80, 1);
	cout << "Checksum" << endl;

	string X(str);
	X.erase(0, 2);  //	1,3,6]
	X.erase(X.size() - 1, 1);	// 1,3,6
	ofstream foTemp("__tempFile");
	foTemp << X;
	foTemp.close();
	ifstream fiTemp("__tempFile");
	
	readHeader();
	ifstream fi(m_FileIn, ios::in | ios::binary);
	//Tao cay huffman
	CHuffNode *root = CreateHuffTree(m_Hd.m_TableCharacter);

	int k = -1;
	char c = '$';
	while (true){
		
		if (c == EOF){
			fiTemp.close();
			DeleteFile("__tempFile");
			break;
		}
		fiTemp >> k;
		c = fiTemp.get();
		ofstream fo(m_Hd.aFile[k - 1].m_FileName, ios::out | ios::binary);
		unsigned long start = m_Hd.aFile[k - 1].m_PosCharFirst;
		unsigned long end = m_Hd.aFile[k - 1].m_PosCharLast;
		fi.seekg(end - 1, ios::beg);	//Dua con tro file ve byte ke cuoi de doc byte cuoi co the chua bit du

		unsigned char c;
		fi.read((char*)&c, sizeof(unsigned char));

		int full_bytes = end - start - 2;

		fi.seekg(start, ios::beg);	//Dua con tro file ve data bat dau

		unsigned char ch = fi.get();
		convertCharToBinary(ch);
		CHuffNode *t = root;
		for (int i = 1; i <= full_bytes;)
		{
			if (t->Left == NULL && t->Right == NULL)
			{
				fo.put(t->c);
				t = root;
			}
			else
			{
				if (m_Buffer.post < 8 && m_Buffer.content[m_Buffer.post] == '0')
					t = t->Left;
				if (m_Buffer.post < 8 && m_Buffer.content[m_Buffer.post] == '1')
					t = t->Right;
				if (m_Buffer.post == 8)
				{
					ch = fi.get();
					convertCharToBinary(ch);
					++i;
					m_Buffer.post = -1;
				}
				m_Buffer.post++;

			}

		}
		if (full_bytes >= 0){
			int remain = (int(c) == 0) ? 8 : 8 - int(c);
			for (; m_Buffer.post <= remain;)
			{
				if (t->Left == NULL && t->Right == NULL)
				{
					fo.put(t->c);
					t = root;
				}
				else
				{
					if (m_Buffer.post < 8 && m_Buffer.content[m_Buffer.post] == '0')
						t = t->Left;
					if (m_Buffer.post < 8 && m_Buffer.content[m_Buffer.post] == '1')
						t = t->Right;
					m_Buffer.post++;
				}
			}
		}
		fo.close();
		checkSum(k - 1);
		cout << endl;
	}
	cout << endl << endl << "Done!";
	delete[]m_Hd.aFile;
	m_Hd.aFile = NULL;
}

void CDecompress::view()
{
	system("cls");
	readHeader();
	cout << "Ten File";
	gotoxy(70, 1);
	cout << "Size truoc nen	";
	gotoxy(90, 1);
	cout << "Size sau nen" << endl;
	for (int i = 0; i < m_Hd.m_Amount; i++){
		cout << i + 1 << ". " << m_Hd.aFile[i].m_FileName;
		gotoxy(70, 2 + i);
		cout << m_Hd.aFile[i].m_SizeBefore;
		gotoxy(90, 2 + i);
		cout << m_Hd.aFile[i].m_SizeEnd << endl;
	}
}

void CDecompress::readHeader()
{
	ifstream fi(m_FileIn, ios::in | ios::binary);
	if (!fi){
		cout << "File khong ton tai";
		exit(0);
	}
	fi.read((char*)&m_Hd.m_Signature, sizeof(m_Hd.m_Signature));	//doc ki tu nhan dang
	if (strcmp(m_Hd.m_Signature, SIGNATURE)){
		cout << "Khong biet file gi";
		exit(0);
	}
	fi.read((char*)&m_Hd.m_Amount, sizeof(m_Hd.m_Amount));	//doc so luong file
	fi.read((char*)&m_Hd.m_TableCharacter, sizeof(m_Hd.m_TableCharacter));	//Doc bang thong ke
	//Doc cac struct file
	m_Hd.aFile = new FILEINFO[m_Hd.m_Amount];
	for (int i = 0; i < m_Hd.m_Amount; i++){
		fi.read((char*)&m_Hd.aFile[i], sizeof(m_Hd.aFile[i]));
	}
	fi.close();
}

void CDecompress::checkSum(unsigned int pos)
{
	static int view = 2;
	if (getFileSize(m_Hd.aFile[pos].m_FileName) == m_Hd.aFile[pos].m_SizeBefore){
		cout << m_Hd.aFile[pos].m_FileName;
		gotoxy(70, view);
		cout << "100%";
		gotoxy(80, view++);
		cout << "0K";
	}
	else{
		cout << m_Hd.aFile[pos].m_FileName;
		gotoxy(70, view);
		cout << "100%";
		gotoxy(80, view++);
		cout << "Fail";
	}
}

unsigned long CDecompress::getFileSize(char* FileName){
	ifstream fi;
	fi.open(FileName, ios::in | ios::binary);
	fi.seekg(0, ios::end);
	return fi.tellg();
}