# Overview:
This is a group project in second year. Its purpose is to encode normal file into .huff file using Huffman algorithm.

***

#How to use:
### Encoding all file in folder_in and put the encoding file in file_out.huf
  - G4 -e folder_in file_out.huf 

### View size before and after encoding
  - G4 -v file_out.huf

###Decoding all file in file_out.huf
  - G4 -d -all file_out.huf

###Decoding specific files in file_out.huf
  - G4 -d -[1,3,6] file_out.huf