#include "HuffTree.h"

CHuffTree::CHuffTree() : Table(256)
{
	root = NULL;
}

CHuffTree::~CHuffTree()
{
	deleteTree(root);
}

CHuffNode* CHuffTree::CreateHuffTree(unsigned __int64 arrTable[256])
{
	CreateQueueTable(arrTable);
	CHuffNode *X, *Y, *Z;
	while (Table.getSize() > 1){
		X = new CHuffNode(Table.deleteMin());
		Y = new CHuffNode(Table.deleteMin());
		Z = new CHuffNode();
		Z->nFreq = X->nFreq + Y->nFreq;
		Z->c = X->c;
		Z->Left = X;
		Z->Right = Y;
		Table.insert(*Z);
	}
	return root = new CHuffNode(Table.deleteMin());
}

void CHuffTree::CreateQueueTable(unsigned __int64 arrTable[256])
{
	for (int i = 0; i < 256; i++){
		if (arrTable[i] > 0){
			CHuffNode *X = new CHuffNode;
			X->Left = NULL;
			X->Right = NULL;
			X->c = i;
			X->nFreq = arrTable[i];
			Table.insert(*X);
		}
	}
}

void CHuffTree::deleteTree(CHuffNode *&root)
{
	if (root == NULL)
		return;
	else{
		deleteTree(root->Left);
		deleteTree(root->Right);
		delete root;
		root = NULL;
	}
}

void CHuffTree::CreateEncodingTable(CHuffNode *p, string *EncodingTable, string temp)
{
	if (p->Left != NULL && p->Right != NULL){
		temp += "0";
		CreateEncodingTable(p->Left, EncodingTable, temp);
		temp.erase(temp.size() - 1, 1);
		temp += "1";
		CreateEncodingTable(p->Right, EncodingTable, temp);
		temp.erase(temp.size() - 1, 1);
	}
	else{
		EncodingTable[p->c] = temp;
	}
}

string* CHuffTree::CreateEnCodingTable(unsigned __int64 arrTable[256])
{
	CreateHuffTree(arrTable);
	CreateEncodingTable(root, EncodingTable);
	return EncodingTable;
}