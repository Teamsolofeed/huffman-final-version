#pragma once
#include "PriorityQueue.h"
#include "HuffNode.h"
#include <iostream>
using namespace std;

class CHuffTree
{
protected:
	CHuffNode *root;
	CPriorityQueue Table;
	string EncodingTable[256];

	void CreateQueueTable(unsigned __int64 arrTable[256]);
	CHuffNode* CreateHuffTree(unsigned __int64 arrTable[256]);
	void deleteTree(CHuffNode *&root);

	void CreateEncodingTable(CHuffNode *p, string *EncodingTable, string temp = "");
public:
	CHuffTree();
	~CHuffTree();

	string* CreateEnCodingTable(unsigned __int64 arrTable[256]);
};

